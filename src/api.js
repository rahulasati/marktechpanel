import axios from "axios";
import { BASE_URL } from "./types";

export default {
  user: {
    login: credentials =>
      axios.post(BASE_URL + "auth/login", { 'email': credentials.email, 'password': credentials.password }).then(res => res.data),
    resetPasswordRequest: password =>
      axios.post("/auth/change_password", { password }).then(res => res.data)
  },
  segment: {
    getSegments: () =>
      axios.get(BASE_URL + 'segment/getSegments').then(res => res.data),
    createSegment: segment =>
      axios.post(BASE_URL + 'segment/createSegment', { ...segment }).then(res => res.data),
    updateSegment: segment =>
      axios.post(BASE_URL + 'segment/updateSegment', { ...segment }).then(res => res.data),
    deleteSegment:(segment_id) =>
      axios.delete(BASE_URL + `segment/delete/${segment_id}`).then(res=>res.data)
  },
  campaign: {
    create: data =>
      axios.post(BASE_URL + 'campaign/create', { ...data }).then(res => res.data),
    getBaseUrls: () =>
      axios.get(BASE_URL + 'campaign/getBaseUrls').then(res => res.data)
  }
};
// axios.get(BASE_URL + 'segment/getSegments' ).then(res => res.data),