import { USER_LOGGED_IN, USER_LOGGED_OUT, SEGMENTS_UPDATED, UPDATE_SEGMENT, SEGMENT_UPDATED, SEGMENT_CREATED, GET_BASE_URLS } from '../types';
import api from "../api";
import setAuthorizationHeader from "../utils/setAuthorizationHeader";

export const loginUser = (credentials) => dispatch =>
    api.user.login(credentials).then(user => {
        localStorage.clientJWT = user.data.token;
        setAuthorizationHeader(localStorage.clientJWT);
        dispatch(userLoggedIn(user.data));
    });

export const logout = () => dispatch => {
    localStorage.removeItem("clientJWT");
    setAuthorizationHeader();
    dispatch(userLoggedOut());
};

export const userLoggedIn = user => ({
    type: USER_LOGGED_IN,
    user
});

export const userLoggedOut = () => ({
    type: USER_LOGGED_OUT
});

export const segmentsRecieved = (segments) => ({
    type: SEGMENTS_UPDATED,
    segments
});

export const updateSegmentClicked = (segment) => ({
    type: UPDATE_SEGMENT,
    segment
});

export const baseUrlsRecieved = (baseUrls) => ({
    type: GET_BASE_URLS,
    baseUrls
});

export const updateSegment = (segment) => dispatch => {
    dispatch(updateSegmentClicked(segment))
}

export const getSegments = () => dispatch =>
    api.segment.getSegments().then(segments => {
        dispatch(segmentsRecieved(segments.data));
    });


export const createSegment = (segment) => dispatch => {
    if (!!segment.id) {
        const seg = { ...segment, segment_id: segment.id }
        return api.segment.updateSegment(seg).then((newsegment) => {
            dispatch({ type: SEGMENT_UPDATED });
        });
    } else {
        return api.segment.createSegment(segment).then((newsegment) => {
            dispatch({ type: SEGMENT_CREATED });
        });
    }
};

export const deleteSegment = (segment_id) => dispatch =>
    api.segment.deleteSegment(segment_id);

export const createCampaign = (data) => dispatch =>
    api.campaign.create(data).then((response) => console.log(response));

export const getBaseUrls = () => dispatch =>
    api.campaign.getBaseUrls().then(segments => {
        dispatch(baseUrlsRecieved(segments.data));
    });