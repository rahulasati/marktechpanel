import React, { Component } from 'react';
import './App.css';

import { Route } from 'react-router-dom';
import { connect } from "react-redux";
import AuthRoute from './routes/AuthRoute';
import GuestRoute from './routes/GuestRoute';

import LoginForm from './components/LoginForm';
import HomePage from './components/HomePage';
import MenuBar from './components/MenuBar';
import SendSMSPage from './components/SendSMSPage';
import SegmentsPage from './components/SegmentsPage';
import CreateSegmentPage from './components/segment/createSegmentPage';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
  appSkeleton = () => {
    return (
      <MuiThemeProvider>
      <div className="ui container App">
        <AuthRoute
          location={this.props.location}
          path="/" exact component={HomePage} />
        <AuthRoute
          location={this.props.location}
          path="/segment" exact component={SegmentsPage} />
        <AuthRoute
          location={this.props.location}
          path="/createsegment" exact component={CreateSegmentPage} />
        <AuthRoute
          location={this.props.location}
          path="/sendsms" exact component={SendSMSPage} />
        <GuestRoute
          location={this.props.location}
          path="/login" exact component={LoginForm} />
      </div>
      </MuiThemeProvider>
    )
  }

  redirect(url) {
    switch (url) {
      case 'home': {
        this.props.history.push('/');
        break;
      }
      case 'segment': {
        this.props.history.push('/segment');
        break;
      }
      case 'sendsms': {
        this.props.history.push('/sendsms');
        break;
      }
    }
  }

  render() {
    return (
      this.props.isAuthenticated ?
        <MenuBar segment={this.appSkeleton()} redirect={this.redirect.bind(this)} /> : this.appSkeleton()
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isAuthenticated: !!state.user.token
  }
}

export default connect(mapStateToProps)(App);
