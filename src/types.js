export const USER_LOGGED_IN = "USER_LOGGED_IN";
export const USER_LOGGED_OUT = "USER_LOGGED_OUT";
//export const BASE_URL = "http://localhost:3000/api/";
export const BASE_URL = "http://54.173.150.242:3005/api/";
export const SEGMENTS_UPDATED = "SEGMENTS_UPDATED";
export const UPDATE_SEGMENT = "UPDATE_SEGMENT";
export const SEGMENT_CREATED = "SEGMENT_CREATED";
export const SEGMENT_UPDATED = "SEGMENT_UPDATED";
export const GET_BASE_URLS = "GET_BASE_URLS";

