import React from 'react'
import { Button, Checkbox, Icon, Table, Confirm, Message } from 'semantic-ui-react'
import { updateSegment, deleteSegment, getSegments } from '../../actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class SegmentTable extends React.Component {

  constructor(props) {
    super(props);
    this.state = { open: false, deleteSegment: null, error: false, message: false }
  }

  redirectToCreateSegment(e) {
    var segment = {}
    if (e.target.id !== 0) {
      const segments = Object.values(this.props.segments);
      console.log(e.target.id);
      let index = segments.findIndex(segment => segment.id == e.target.id)
      console.log(index);
      let tempsegment = segments[index] || {};
      segment = {
        id: tempsegment.id || null,
        name: tempsegment.segment_name,
        desc: tempsegment.segment_desc,
        query: tempsegment.segment_query
      }
    }
    this.props.updateSegment(segment);
    this.props.history.push('/createsegment');
  }

  showDeleteModal(e) {
    console.log(e.target.id);
    this.setState({ open: true, deleteSegment: e.target.id })
  }

  handleConfirm = () => {
    this.props.deleteSegment(this.state.deleteSegment).then((res) => {
      this.setState({ message: res.message, open: false, deleteSegment: null }, function () {
        setTimeout(() => {
          this.setState({ message: false })
        }, 2000);
        this.props.getSegments();
      });
    })
      .catch((err) => {
        this.setState({
          open: false,
          deleteSegment: null,
          loading: false,
          error: {
            global: err.response.data.message
          }
        })
        setTimeout(() => {
          this.setState({ error: false })
        }, 2000)
      });
  }
  handleCancel = () => this.setState({ open: false, deleteSegment: null })


  render() {
    const segments = Object.values(this.props.segments);
    const listItems = (segments.length === 0) ? <Table.Row>
      <Table.Cell colSpan='5'>No Segments Created</Table.Cell>
    </Table.Row> : segments.map((segment) => (
      <Table.Row key={segment.id}>
        <Table.Cell>{segment.segment_name}</Table.Cell>
        <Table.Cell>{segment.segment_desc}</Table.Cell>
        <Table.Cell>{segment.segment_query}</Table.Cell>
        <Table.Cell>{segment.created_by}</Table.Cell>

        <Table.Cell>
          <Button size='tiny'
            id={segment.id}
            onClick={this.redirectToCreateSegment.bind(this)}
            color="blue">Edit</Button>

          <Button style={{ 'marginLeft': '2px' }}
            id={segment.id}
            onClick={this.showDeleteModal.bind(this)}
            size='tiny' color="red">Delete</Button>
        </Table.Cell>

      </Table.Row>
    )
    );

    return (
      <div>
        <Confirm
          open={this.state.open}
          content="Are you sure you want to delete this Segment?"
          onCancel={this.handleCancel}
          onConfirm={this.handleConfirm}
        />
        {
          !!this.state.message && <Message success >
            {this.state.message}
          </Message>
        }
        {
          !!this.state.error.global && <Message error >
            {this.state.error.global}
          </Message>
        }
        <Table celled compact definition stackable>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell width={2} >Segment Name</Table.HeaderCell>
              <Table.HeaderCell width={2} >Segment Description</Table.HeaderCell>
              <Table.HeaderCell width={6} >Segment Query</Table.HeaderCell>
              <Table.HeaderCell width={2} >Created By</Table.HeaderCell>
              <Table.HeaderCell width={3} >Actions</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {listItems}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan='6'>
                <Button
                  id={0}
                  onClick={this.redirectToCreateSegment.bind(this)}
                  floated='right' icon labelPosition='left' primary size='small'>
                  <Icon name='users' /> Add new Segment
            </Button>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    segments: state.segments.segments
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    updateSegment: updateSegment,
    deleteSegment: deleteSegment,
    getSegments: getSegments
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(SegmentTable);