import React, { Component } from "react";
import {
  Button,
  Form,
  Icon,
  Message,
  TextArea,
  Container,
  Header
} from "semantic-ui-react";
import { connect } from "react-redux";
import { updateSegment,createSegment } from '../../actions';
import {bindActionCreators} from 'redux';

class CreateSegmentPage extends Component {

    constructor(props) {
        super(props);
        this.state = {loading:false,error:{}};
    }

    handleChange(e) {
        this.props.updateSegment({...this.props.segment, [e.target.name]:e.target.value })
    }

    handleDismiss() {
      this.setState({ error: {} })
    }

    validate(segment) {
      var error = {};
      if(!segment.name) error.name = 'Segment Name cannot be blank';
      if(!segment.query) error.query = 'Query cannot be blank';
      return error;
    }

    createSegment(e){
      e.preventDefault();
      const segment = this.props.segment;
      const error = this.validate(segment);
      this.setState({error})
      if(Object.keys(error).length === 0) {
        this.setState({loading:true});
        this.props.createSegment(segment).then(() => this.props.history.push('/segment'))
        .catch((err)=>{
          this.setState({loading:false,
            error:{
              global: err.response.data.message
            }
          })
          setTimeout(() => {
            this.setState({ error: {} })
          }, 5000)
        });
      }
    }


  render() {
    const {segment} = this.props;
    const {loading,error} = this.state;
    return (
      <Container>
        <Header />
        <Message attached header="Enter Segment details" />
        {
          !!error.global && <Message
          error
          floating
          onDismiss={this.handleDismiss.bind(this)}
          header='Error'
          content={error.global}
          />
  
        }
        <Form className="attached fluid segment rounded compact"
          loading={loading}>
          <Form.Group widths="equal">
            <Form.Input placeholder="Segment Name"
            name='name'
            value={segment.name}
            onChange={this.handleChange.bind(this)}
            error={!!error.name}
             type="text" />
          </Form.Group>
          {!!error.name && 
            <span style={{color:"#ae5856"}}>
            {error.name}
            </span>}
          <Form.Field control={TextArea}
            name='desc'
            value={segment.desc}
            onChange={this.handleChange.bind(this)}
           placeholder="Description..." />
          <Form.Field control={TextArea} 
          name='query'
          value={segment.query}
          onChange={this.handleChange.bind(this)}
          error={!!error.query}
          placeholder="Type Query Here..." />
          {
            !!error.query &&
            <span style={{color:"#ae5856"}}>
            {error.query}
            </span>
          }
          <Form.Group>
            <Button fluid color="blue"
            onClick={this.createSegment.bind(this)}
            >
              Submit
            </Button>
          </Form.Group>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
        segment: state.segments.segment
    }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    updateSegment: updateSegment,
    createSegment: createSegment
  }, dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(CreateSegmentPage);
