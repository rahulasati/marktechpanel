import React, { Component } from 'react'
import { Sidebar, Container, Menu, Button, Image, Icon, Header } from 'semantic-ui-react';

import { connect } from "react-redux";
import { Route, Redirect } from 'react-router-dom';

import * as actions from '../actions';

class MenuBar extends Component {
  state = { visible: false }

  toggleVisibility = () => this.setState({ visible: !this.state.visible })

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name, visible: !this.state.visible });
    this.props.redirect(name);
  }

  render() {
    const { visible, activeItem } = this.state;
    const { isAuthenticated } = this.props
    return (
      <div>
        <Sidebar.Pushable as={Menu}
          style={{ height: '100%' }}
        >
          <Sidebar as={Menu} animation='push' width='thin' visible={visible} icon='labeled'
            vertical
            inverted>
            <Menu.Item
              name='home'
              active={activeItem === 'home'}
              onClick={this.handleItemClick}
            >
              <Icon name='home' />
              Home
            </Menu.Item>
            <Menu.Item
              name='segment'
              active={activeItem === 'segment'}
              onClick={this.handleItemClick}
            >
              <Icon name='users' />
              Segments
            </Menu.Item>
            <Menu.Item
              name='sendsms'
              active={activeItem === 'sendsms'}
              onClick={this.handleItemClick}
            >
              <Icon name='mobile' />
              Campaigns
            </Menu.Item>
          </Sidebar>
          <Sidebar.Pusher
            style={{ minHeight: '100vh', minWidth: '100vw' }}
          >
            <div>
              <Menu>
                <Menu.Item header name='Menu' onClick={this.toggleVisibility} />
                <Menu.Menu position='right'>
                  <Menu.Item name='logout' onClick={() => this.props.logout()}>
                    Logout
          </Menu.Item>
                </Menu.Menu>
              </Menu>
              {this.props.segment}
            </div>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isAuthenticated: !!state.user.token
  }
}

export default connect(mapStateToProps, { logout: actions.logout })(MenuBar);