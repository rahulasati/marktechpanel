import React, {Component} from 'react';
import { Loader, Button, Form, Grid, Header, Image, Message, Segment, Label } from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import { loginUser } from '../actions/index';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import axios from 'axios';
import Validator from 'validator';

class LoginForm extends Component {

  constructor() {
    super();

    this.state = {
      data : {
        email: '',
        password: '',  
      },
      loading:false,
      error:{}
    }

  }

  loginUser(user) {
    this.setState({loading:true})
    // axios.post('/auth/login',{email:user.email, password:user.password}).then((res) => {
    //   this.setState({loading:false})
    //   console.log(res);
    //   this.props.history.push('/');
    // })
    const credentials = {email:user.email, password:user.password};
    this.props.loginUser(credentials).then(() => {
    this.props.history.push('/')
  })
    .catch((err) => {
      console.log(err);
      console.log(err.response);
      this.setState({loading:false,
        error:{
          global: err.response.message
        }
      })
      setTimeout(() => {
        this.setState({ error: false })
      }, 5000)
    })
  }

  handleSubmit(e) {
    e.preventDefault();
    const user = this.state.data;
    const error = this.validate(this.state.data);
    this.setState({error})
    if(Object.keys(error).length === 0) {
      this.loginUser(user);
    }
  }

  validate(user) {
    var error = {};
    if(!Validator.isEmail(user.email)) error.email = 'Invalid email format';
    if(!user.password) error.password = 'Password cannot be blank';
    return error;
  }

  handleOnChange(e) {
    this.setState({data : {...this.state.data, [e.target.name] : e.target.value}});
  }

  handleDismiss() {
    this.setState({ error: {} })
  }

  render() {
    var {data, loading, error} = this.state;
    return (
      <div className='login-form'>
      <style>{`
        body > div,
        body > div > div,
        body > div > div > div.login-form {
          height: 100%;
        }
      `}</style>
      <Grid
        textAlign='center'
        style={{ height: '100%' }}
        verticalAlign='middle'
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' color='teal' textAlign='center'>
            <Image src='https://media.go2app.org/user_content/brand/logos/conversionx/logo_1510834735.png' size='huge'>
            </Image>
            {' '}Log-in to your account
          </Header>
          <Form size='large'>
            <Segment stacked>
              <Form.Input
                fluid
                icon='user'
                iconPosition='left'
                placeholder='E-mail address'
                name='email'
                value={data.email}
                error={!!error.email}
                onChange={this.handleOnChange.bind(this)}
              />
              {error.email && 
              <span style={{color:"#ae5856"}}>
              {error.email}
              </span>}
              <Form.Input
                fluid
                icon='lock'
                iconPosition='left'
                placeholder='Password'
                type='password'
                name='password'
                value={data.password}
                error={!!error.password}
                onChange={this.handleOnChange.bind(this)}
              />
              {error.password && 
                <span style={{color:"#ae5856"}}>
                {error.password}
                </span>}
              <Button color='teal' fluid size='large' 
              loading={loading}
              onClick={this.handleSubmit.bind(this)}>Login</Button>
            </Segment>
          </Form>
          {
            !!error.global && <Message
            error
            floating
            onDismiss={this.handleDismiss.bind(this)}
            header='Error'
            content={error.global}
            />
    
          }
          <Message>
            Forgot Password?
          </Message>
        </Grid.Column>
      </Grid>
    </div>  
    )
  }
}


function matchDispatchToProps(dispatch) {
   return bindActionCreators({loginUser: loginUser}, dispatch);
}

export default connect(null,matchDispatchToProps)(LoginForm);