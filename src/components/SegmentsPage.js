import React from "react";
import { Link } from "react-router-dom";

import { getSegments } from '../actions/index.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import SegmentTable from "./segment/SegmentTable";

import {
  Loader,
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
  Label,
  Dimmer
} from "semantic-ui-react";

class SegmentsPage extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      error: {}
    };
  }

  getSegment() {
    this.setState({ loading: true });
    this.props.getSegments().then(() => {
      this.setState({ loading: false, error: {} });
      console.log("segments fetched successfully..");
    })
      .catch((err) => {
        this.setState({
          loading: false,
          error: {
            global: err.message
          }
        })
        setTimeout(() => {
          this.setState({ error: {} })
        }, 5000)
      })
  }

  componentWillMount() {
    this.getSegment()
  }

  handleDismiss() {
    this.setState({ error: {} })
  }

  render() {
    var { data, loading, error } = this.state;
    return (
      <div>
        <Grid
          centered
          padded={true}
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column>
            {
              !!error.global && <Message
                error
                floating
                onDismiss={this.handleDismiss.bind(this)}
                header='Error'
                content={error.global}
              />

            }
            <Dimmer inverted active={loading}>
              <Loader>Loading...</Loader>
            </Dimmer>
            <Header>Segments:</Header>
            <SegmentTable history={this.props.history} />
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    segments: state.segments
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ getSegments: getSegments }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(SegmentsPage);
