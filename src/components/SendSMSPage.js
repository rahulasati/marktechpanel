import React, { Component } from 'react'
import { Button, Form, Message, Checkbox, Label, TextArea, Select, Container, Header } from 'semantic-ui-react'
import { getSegments, getBaseUrls, createCampaign } from '../actions/index.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DateTimePicker from 'material-ui-datetimepicker';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog'
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';

class SendSMSPage extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                segment_id: '',
                sender_id: '',
                message: '',
                time: -1,
                send_by: 'segment',
                numbers: '',
                charCount: 0,
                messageCount: 0,
            },
            error: {},
            datePickerDialog: null
        };
    }

    getSegment() {
        this.props.getSegments().then(() => {
            console.log('segments received');
        })
            .catch((err) => {
                console.log('error while get segments' + err);
            })
    }

    getBaseUrls() {
        this.props.getBaseUrls().then(() => {
            console.log('getBaseUrls received');
        })
            .catch((err) => {
                console.log('error while getBaseUrls' + err);
            })
    }

    createCampaignBySegments() {
        console.log('createCampaignBySegments');
        const error = this.validate(this.state.data);
        this.setState({ error });
        if (Object.keys(error).length === 0) {
            var currMillis = Math.round((new Date()).getTime() / 1000);
            console.log(currMillis);
            this.setState({ data: { ...this.state.data, ['time']: currMillis } }, () => {
                console.log(this.state.data);
                this.props.createCampaign(this.state.data).then(() => {
                    this.setState({
                        data: {
                            segment_id: '',
                            sender_id: '',
                            message: '',
                            time: -1,
                            send_by: 'segment',
                            numbers: '',
                            charCount: 0,
                            messageCount: 0,
                            segment_name: '',
                            base_url: '',
                            url_path: '',
                            redirect_url: '',
                            tags: ''
                        },
                        error: {},
                        dateTime: null
                    });
                    window.alert('SMS Scheduled Successfully.');
                    //console.log('SMS Scheduled Successfully.');
                }).catch((err) => {
                    console.log('error while Scheduling : ' + err);
                })
            });
        }
    }

    validate(formData) {
        var error = {};
        if (formData.send_by === 'segment') {
            formData.numbers = '';
            if (!formData.segment_id) error.segment_id = 'Please Select Segment';
        } else {
            formData.segment_id = '';
            if (!formData.numbers) error.numbers = 'Phone Number can not be blank';
        }
        if (!formData.sender_id) error.sender_id = 'SenderID can not be blank';
        if (!formData.message) error.message = 'Message can not be blank';

        if (!formData.campaign_name) error.campaign_name = 'Campaign Name can not be blank';
        if (!formData.base_url) error.base_url = 'Please Select Link Base URL.';
        if (!formData.url_path) error.url_path = 'Link URL Short Word can not be blank';
        if (!formData.redirect_url) error.redirect_url = 'Redirect URL can not be blank';
        return error;
    }

    componentWillMount() {
        this.getSegment();
        this.getBaseUrls();
    }

    handleSegmentChange = (e, { value }) => {
        this.setState({ data: { ...this.state.data, ['segment_id']: value } });
    }

    handleUrlChange = (e, { value }) => {
        this.setState({ data: { ...this.state.data, ['base_url']: value } });
    }

    handleRadioChange = (e, { value }) => {
        this.setState({ data: { ...this.state.data, ['send_by']: value } });
    }

    handleOnChange(e) {
        this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } }, () => {
            var message = this.state.data.message;
            const charCount = message.length;
            const messageCount = Math.ceil(charCount / 160);
            this.setState({ data: { ... this.state.data, charCount: charCount, messageCount: messageCount } })
        });
    }

    handleScheduleClick = (e) => {
        e.preventDefault();
        console.log("handleScheduleClick");
        const formData = this.state.data;
        console.log(formData);
        const error = this.validate(formData);
        this.setState({ error })
        if (Object.keys(error).length === 0) {
            this.state.datePickerDialog.openDatePicker(e);
        }
    }

    handleSendClick = (e) => {
        e.preventDefault();
        console.log("handleSendClick");
        const formData = this.state.data;
        console.log(formData);
    }

    setDatePickerInput = (ref) => {
        this.setState({ datePickerDialog: ref });
    }

    onDateTimeChange = (selectedDate) => {
        console.log(selectedDate);
        console.log(selectedDate.unix());
        this.setState({ data: { ...this.state.data, ['time']: selectedDate.unix() } }, () => {
            const formData = this.state.data;
            console.log('-------------------------------')
            console.log(formData);
            this.props.createCampaign(formData).then(() => {
                this.setState({
                    data: {
                        segment_id: '',
                        sender_id: '',
                        message: '',
                        time: -1,
                        send_by: 'segment',
                        numbers: '',
                        segment_name: '',
                        base_url: '',
                        url_path: '',
                        redirect_url: '',
                        tags: ''
                    },
                    error: {},
                    dateTime: null
                });
                window.alert('SMS Scheduled Successfully.');
                //console.log('SMS Scheduled Successfully.');
            }).catch((err) => {
                console.log('error while Scheduling : ' + err);
            })
        });
    }

    render() {
        var data = this.state.data;
        var error = this.state.error;
        console.log(data);
        const segments = Object.values(this.props.segments);
        const options = segments.map(segment => { return { text: segment.segment_name, value: segment.id } });
        const baseUrls = Object.values(this.props.baseUrls);
        const baseUrlOptions = baseUrls.map(baseUrls => { return { text: baseUrls.name, value: baseUrls.name } });
        console.log(baseUrlOptions);
        return (
            <div>
                <Container>
                    <Header />
                    <Message attached header="Create Campaign" />
                    <Form className='attached fluid segment'>

                        <Form.Group style={{ 'paddingBottom': '10px' }}>
                            <Form.Field>
                                <Checkbox
                                    radio
                                    label='Segment'
                                    name='checkboxRadioGroup'
                                    value='segment'
                                    checked={data.send_by === 'segment'}
                                    onChange={this.handleRadioChange}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Checkbox
                                    radio
                                    label='Numbers'
                                    name='checkboxRadioGroup'
                                    value='numbers'
                                    disabled='false'
                                    checked={data.send_by === 'numbers'}
                                    onChange={this.handleRadioChange}
                                />
                            </Form.Field>
                        </Form.Group >

                        {
                            data.send_by === 'segment' && <Form.Field error={!!error.segment_id} control={Select} value={data.segment_id} options={options} placeholder='Select Segment' onChange={this.handleSegmentChange.bind(this)} />
                            //{error.segment_id && <span style={{ color: "#ae5856" }} /> }
                        }

                        {
                            data.send_by === 'numbers' &&
                            <div style={{ 'paddingBottom': '10px' }}>

                                <Form.Group style={{ 'paddingLeft': '10px' }}>
                                    <span style={{ float: 'left' }}> <strong>Note : </strong> For multiple numbers, use "COMMA" to seperate numbers. &nbsp; <strong> Example: </strong> 981XXXXXXX,981XXXXXXX</span>
                                </Form.Group>

                                <Form.Field error={!!error.numbers} name='numbers' value={data.numbers} control={TextArea} placeholder='Phone Numbers' onChange={this.handleOnChange.bind(this)} />

                            </div>
                        }

                        <Form.Input error={!!error.campaign_name} name='campaign_name' value={data.campaign_name} placeholder='Campaign Name' type='text' onChange={this.handleOnChange.bind(this)} />
                        {error.campaign_name && <span style={{ color: "#ae5856" }} />}
                        
                        <Form.Field error={!!error.base_url} control={Select} value={data.base_url} options={baseUrlOptions} placeholder='Select Link Base URL' onChange={this.handleUrlChange.bind(this)} />

                        <Form.Input error={!!error.url_path} name='url_path' value={data.url_path} placeholder='Link Short Word' type='text' onChange={this.handleOnChange.bind(this)} />
                        {error.url_path && <span style={{ color: "#ae5856" }} />}

                        <Form.Input error={!!error.redirect_url} name='redirect_url' value={data.redirect_url} placeholder='Redirect URL' type='text' onChange={this.handleOnChange.bind(this)} />
                        {error.redirect_url && <span style={{ color: "#ae5856" }} />}

                        <Form.Input error={!!error.sender_id} name='sender_id' value={data.sender_id} placeholder='SenderID' type='text' onChange={this.handleOnChange.bind(this)} />
                        {error.sender_id && <span style={{ color: "#ae5856" }} />}

                        <Form.Field error={!!error.message} name='message' value={data.message} control={TextArea} placeholder='Type Message Here...' onChange={this.handleOnChange.bind(this)} />
                        {error.message && <span style={{ color: "#ae5856" }} />}

                        <Form.Input error={!!error.tags} name='tags' value={data.tags} placeholder='Tags (Optional)' type='text' onChange={this.handleOnChange.bind(this)} />
                        {error.tags && <span style={{ color: "#ae5856" }} />}

                        <Form.Group style={{ 'paddingLeft': '10px', 'paddingTop': '10px', 'paddingBottom': '10px'}}>
                            <span style={{ float: 'left' }}> <strong> Characters Count </strong>  : {data.charCount} &nbsp;&nbsp;&nbsp;&nbsp; <strong> Message Count </strong>  : {data.messageCount} </span>
                        </Form.Group>

                        <Form.Group widths='equal' style={{ 'paddingLeft': '10px' }} >
                            <Button color='blue' onClick={this.handleScheduleClick} >Schedule</Button>
                            <Button style={{ 'margin-left':'10px'}} color='blue' onClick={this.createCampaignBySegments.bind(this)} >Send Now</Button>
                        </Form.Group >
                    </Form>
                </Container>

                <DateTimePicker
                    clearIcon={null}
                    returnMomentDate={true}
                    textFieldStyle={{ display: 'none' }}
                    ref={this.setDatePickerInput}
                    DatePicker={DatePickerDialog}
                    TimePicker={TimePickerDialog}
                    onChange={this.onDateTimeChange} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        segments: state.segments.segments,
        baseUrls: state.baseUrls
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ getSegments: getSegments, getBaseUrls: getBaseUrls, createCampaign: createCampaign }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(SendSMSPage);