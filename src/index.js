import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import thunk from 'redux-thunk';
import decode from 'jwt-decode';
import registerServiceWorker from './registerServiceWorker';
import setAuthorizationHeader from './utils/setAuthorizationHeader';

import {BrowserRouter, Route} from 'react-router-dom';

import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';

import allReducers from './reducers';

import { userLoggedIn } from "./actions";

const store = createStore(allReducers,applyMiddleware(thunk));

if (localStorage.clientJWT) {
    const payload = decode(localStorage.clientJWT);
    console.log('index.js root -');
    console.log(payload);
    console.log(localStorage.clientJWT);
    const user = {
      token: localStorage.clientJWT,
      user_id: payload.id,
    };
    setAuthorizationHeader(localStorage.clientJWT);
    store.dispatch(userLoggedIn(user));
  }

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
    <Route component={App} />
    </Provider>
    </BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
