import { combineReducers } from 'redux';
import user from './user.js';
import segments from './segments.js';
import baseUrls from './baseUrls.js';

const allReducers = combineReducers({ user, segments, baseUrls });

export default allReducers