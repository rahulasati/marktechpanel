import {SEGMENTS_UPDATED, UPDATE_SEGMENT} from '../types';

export default function segments(state={segments:[],segment:{}}, action={}) {
    switch(action.type) {
        case SEGMENTS_UPDATED:
            return {...state,segments: action.segments};
        case UPDATE_SEGMENT:
            return {...state, segment: action.segment};
        default:
            return state;
    }
}