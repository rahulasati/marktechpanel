import { GET_BASE_URLS } from '../types';

export default function baseUrls(state = {}, action = {}) {
    switch (action.type) {
        case GET_BASE_URLS:
            return action.baseUrls;
        default:
            return state;
    }
}