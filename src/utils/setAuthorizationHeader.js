import axios from 'axios';

export default (token = null) => {
    if(token) {
        axios.defaults.headers.access_token = token
    } else {
        delete axios.defaults.headers.access_token
    }
};